package gologger

import (
	"os"
	"strings"

	"github.com/sirupsen/logrus"
)

// NewLogrusLoggerFromEnvironment creates a new LogrusLogger
// and configure it based on the current environment variables
// It picks up the following env
// - LOG_ENV -> default to Info
func NewLogrusLoggerFromEnvironment() *LogrusLogger {
	logger := NewLogrusLogger()
	logLevel := os.Getenv("LOG_LEVEL")
	if logLevel != "" {
		logger.SetLevel(Level(logLevel))
	}
	return logger
}

// NewLogrusLogger creates a new LogrusLogger
func NewLogrusLogger() *LogrusLogger {
	logger := logrus.New()
	entry := logrus.NewEntry(logger)

	logger.SetLevel(logrus.InfoLevel)

	return &LogrusLogger{
		logger: logger,
		entry:  entry,
		tags:   make([]string, 0),
	}
}

// LogrusLogger implements the Logger interface using Logrus
type LogrusLogger struct {
	logger *logrus.Logger
	entry  *logrus.Entry
	tags   []string
}

var logrusLevelMap = map[Level]logrus.Level{
	Debug: logrus.DebugLevel,
	Info:  logrus.InfoLevel,
	Warn:  logrus.WarnLevel,
	Error: logrus.ErrorLevel,
}

// SetLevel sets the log level. Default is Info
func (l *LogrusLogger) SetLevel(level Level) {
	l.logger.SetLevel(logrusLevelMap[level])
	l.entry = logrus.NewEntry(l.logger)
}

// SetTags sets the `tags` field for every log entries
func (l *LogrusLogger) SetTags(tags ...string) {
	if len(tags) == 0 {
		return
	}

	l.entry = l.logger.WithField("tags", strings.Join(tags, ","))
	l.tags = tags
}

// Debug prints debug level messages
func (l *LogrusLogger) Debug(format string, args ...interface{}) {
	l.entry.Debugf(format+"\n", args...)
}

// Info prints info level messages
func (l *LogrusLogger) Info(format string, args ...interface{}) {
	l.entry.Infof(format+"\n", args...)
}

// Warn prints warning level messages
func (l *LogrusLogger) Warn(format string, args ...interface{}) {
	l.entry.Warnf(format+"\n", args...)
}

// Error prints error level messages
func (l *LogrusLogger) Error(format string, args ...interface{}) {
	l.entry.Errorf(format+"\n", args...)
}

// Fields sets new `fields`, and it also carries `tags` over if set
func (l *LogrusLogger) Fields(fields Fields) Logger {
	newFields := make(map[string]interface{})

	existingFields := l.entry.Data
	// copy existing fields over
	for k, v := range existingFields {
		newFields[k] = v
	}

	// merge new fields with existing fields
	for k, v := range fields {
		newFields[k] = v
	}

	if len(l.tags) != 0 {
		fields["tags"] = strings.Join(l.tags, ",")
	}

	entry := l.logger.WithFields(newFields)

	return &LogrusLogger{
		logger: l.logger,
		entry:  entry,
		tags:   l.tags,
	}
}

// Field is the same as Fields but only set 1 key at a time
func (l *LogrusLogger) Field(field string, value interface{}) Logger {
	fields := make(map[string]interface{})
	fields[field] = value
	return l.Fields(fields)
}
