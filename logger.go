package gologger

// Level represents the log level
type Level string

// Debug level is usually used for development/testing. Very verbose with a lot of info
const Debug Level = "debug"

// Info level. General operational entries about what's going on inside the application
const Info Level = "info"

// Warn level. Non-critical entries that deserve eyes
const Warn Level = "warn"

// Error level. Used for errors that should definitely be noted.
const Error Level = "error"

// Fields is any kind of structure that one can pass to the logging functions
type Fields map[string]interface{}

// Logger defines methods to "log" stuff
type Logger interface {
	SetLevel(level Level)
	SetTags(tags ...string)

	Debug(format string, args ...interface{})
	Info(format string, args ...interface{})
	Warn(format string, args ...interface{})
	Error(format string, args ...interface{})

	Fields(fields Fields) Logger
	Field(field string, value interface{}) Logger
}
